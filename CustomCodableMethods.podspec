Pod::Spec.new do |s|
  s.default_subspec  = "Core"
  s.name             = "CustomCodableMethods"
  s.version          = "0.0.1"
  s.summary          = "Custom methods for JSONDecoder or JSONEncoder"
  s.homepage         = "https://bitbucket.org/pavelsmelovsky/customcodablemethods/overview"
  s.license          = 'MIT'
  s.author           = { "Pavel Smelovsky" => "pavel.smelovsky@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/pavelsmelovsky/customcodablemethods.git", :tag => "v#{s.version}" }
  s.social_media_url = 'https://twitter.com/PavelSmelovsky'

  s.swift_versions        = '5.0'
  s.pod_target_xcconfig   = { "SWIFT_VERSION" => "5.0" }
  
  #s.platform     = :ios, '8.0'
  s.ios.deployment_target = '9.0'
  s.osx.deployment_target = '10.12'

  s.requires_arc = true

  s.module_name   = 'CustomCodableMethods'

  #s.resources = 'Pod/Assets/*'

  s.subspec 'Core' do |sp|
    sp.frameworks    = 'Foundation'

    sp.source_files  = 'CustomCodableMethods/CustomCodableMethods/Core/**/*.{swift,h,m}'
  end

end

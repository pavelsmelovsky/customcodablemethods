//
//  CustomCodableMethodsTests.swift
//  CustomCodableMethodsTests
//
//  Created by Pavel Smelovsky on 11/06/2019.
//  Copyright © 2019 Pavel Smelovsky. All rights reserved.
//

import XCTest
@testable import CustomCodableMethods

class CustomCodableMethodsTests: XCTestCase {
    private lazy var bundle = Bundle(for: CustomCodableMethodsTests.self)

    private var data: Data!

    override func setUp() {
        guard let url = self.bundle.url(forResource: "test", withExtension: "json") else { fatalError() }
        do {
            let string = try String.init(contentsOf: url)
            self.data = string.data(using: .utf8)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testJsonDecoding() {
        XCTAssertNoThrow(try self.parseObject())
    }

    private func parseObject() throws {
        let object = try JSONDecoder().decode(TestDecodable.self, from: self.data)

        guard case JSON.dictionary(let dict) = object.dictionary else { throw ParseError.invalidType("root") }

        try self.checkDictionary(dict)

        let internalDict = try self.value(from: dict, of: [String: JSON].self, forKey: "internal-dictionary")
        try self.checkDictionary(internalDict)
    }

    private func checkDictionary(_ dict: [String: JSON]) throws {
        let integer = try self.value(from: dict, of: Int.self, forKey: "integer")
        XCTAssertEqual(integer, 123)

        let uint = try self.value(from: dict, of: UInt.self, forKey: "unsigned-integer")
        XCTAssertEqual(uint, 9223372036854775808)

        let double = try self.value(from: dict, of: Double.self, forKey: "double")
        XCTAssertEqual(double, 423423.2132)

        let string = try self.value(from: dict, of: String.self, forKey: "string")
        XCTAssertEqual(string, "Some string value")

        let boolean = try self.value(from: dict, of: Bool.self, forKey: "boolean")
        XCTAssertEqual(boolean, true)

        let sourceArrayOfIntegers = [0,1,2,3,4,5,6,7,8,9]
        let arrayOfIntegers = try self.value(from: dict, of: [JSON].self, forKey: "array-of-integers")
        XCTAssertEqual(arrayOfIntegers.count, sourceArrayOfIntegers.count)
        for index in 0..<arrayOfIntegers.count {
            guard case .int(let itemInt) = arrayOfIntegers[index] else { throw ParseError.invalidType("array-of-integers[\(index)]") }

            XCTAssertEqual(sourceArrayOfIntegers[index], itemInt)
        }

        // test array of dict
        // "array-of-dict": [{"id": 432}, {"id": 6543}, {"other object": "fdsafasd"}]
        let arrayOfDict = try self.value(from: dict, of: [JSON].self, forKey: "array-of-dict")
        XCTAssertEqual(arrayOfDict.count, 3)

        guard case .dictionary(let firstDict) = arrayOfDict[0] else { throw ParseError.invalidType("array-of-dict[0]")}
        XCTAssertEqual(try self.value(from: firstDict, of: Int.self, forKey: "id"), 432)

        guard case .dictionary(let secondDict) = arrayOfDict[1] else { throw ParseError.invalidType("array-of-dict[1]")}
        XCTAssertEqual(try self.value(from: secondDict, of: Int.self, forKey: "id"), 6543)

        guard case .dictionary(let thirdDict) = arrayOfDict[2] else { throw ParseError.invalidType("array-of-dict[2]")}
        XCTAssertEqual(try self.value(from: thirdDict, of: String.self, forKey: "other object"), "fdsafasd")
    }

    private func value<T>(from dict: [String: JSON], of aType: T.Type, forKey key: String) throws -> T {
        guard let object = dict[key] else { throw ParseError.unknownKey(key) }

        switch object {
        case .null:
            guard NSNull.self == aType else { throw ParseError.invalidType(key) }
            return NSNull() as! T
        case .int(let value):
            guard type(of: value) == aType else { throw ParseError.invalidType(key) }
            return value as! T
        case .bool(let value):
            guard type(of: value) == aType else { throw ParseError.invalidType(key) }
            return value as! T
        case .double(let value):
            guard type(of: value) == aType else { throw ParseError.invalidType(key) }
            return value as! T
        case .uint(let value):
            guard type(of: value) == aType else { throw ParseError.invalidType(key) }
            return value as! T
        case .string(let value):
            guard type(of: value) == aType else { throw ParseError.invalidType(key) }
            return value as! T
        case .array(let value):
            guard type(of: value) == aType else { throw ParseError.invalidType(key) }
            return value as! T
        case .dictionary(let value):
            guard type(of: value) == aType else { throw ParseError.invalidType(key) }
            return value as! T
        }
    }
}

enum ParseError: Error {
    case invalidType(String)
    case unknownKey(String)
}

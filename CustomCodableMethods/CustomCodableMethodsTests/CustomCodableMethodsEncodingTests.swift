//
//  CustomCodableMethodsEncodingTests.swift
//  CustomCodableMethodsTests
//
//  Created by Pavel Smelovsky on 12/06/2019.
//  Copyright © 2019 Pavel Smelovsky. All rights reserved.
//

import XCTest
@testable import CustomCodableMethods

class CustomCodableMethodsEncodingTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testEncoding() {
        XCTAssertNoThrow(try self.parseObject())
    }

    func parseObject() throws {
        let object = self.createObject()
        let data = try self.parseToData(from: object)

        let restoredObject = try self.restoreObject(from: data)

        XCTAssertEqual(object.name, restoredObject.name)

        XCTAssertEqual(object.array, restoredObject.array)
        XCTAssertEqual(object.dictionary, restoredObject.dictionary)
    }

    func parseToData(from object: TestEncoding) throws -> Data {
        return try JSONEncoder().encode(object)
    }

    func restoreObject(from data: Data) throws -> TestEncoding {
        return try JSONDecoder().decode(TestEncoding.self, from: data)
    }

    private func createObject() -> TestEncoding {
        let arrayOfInteger: [JSON] = [
            .int(0), .int(1), .int(2), .int(3), .int(4),
            .int(5), .int(6), .int(7), .int(8), .int(9),
        ]

        let internalDict: [String: JSON] = [
            "integer": .int(123),
            "arrayOfInteger": .array(arrayOfInteger)
        ]

        let dict = JSON.dictionary([
            "integer": .int(123),
            "uinteger": .uint(9376497310422336528),
            "string": .string("string"),
            "arrayOfInteger": .array(arrayOfInteger),
            "booleanFalse": .bool(false),
            "booleanTrue": .bool(true),
            "nullableValue": .null,
            "dictionary": .dictionary(internalDict)
            ])
        let array = JSON.array([
            .int(1), .string("string"),
            .dictionary(["double": .double(543532.56423)])
            ])
        return TestEncoding(name: "name", dictionary: dict, array: array)
    }
}

//
//  TestEncoding.swift
//  CustomCodableMethodsTests
//
//  Created by Pavel Smelovsky on 12/06/2019.
//  Copyright © 2019 Pavel Smelovsky. All rights reserved.
//

import Foundation
@testable import CustomCodableMethods

struct TestEncoding: Codable {
    var name: String
    var dictionary: JSON
    var array: JSON
}

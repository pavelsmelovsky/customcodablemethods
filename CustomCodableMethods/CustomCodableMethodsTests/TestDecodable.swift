//
//  TestDecodable.swift
//  CustomCodableMethodsTests
//
//  Created by Pavel Smelovsky on 11/06/2019.
//  Copyright © 2019 Pavel Smelovsky. All rights reserved.
//

import Foundation
@testable import CustomCodableMethods

struct TestDecodable: Decodable {
    var id: Int
    var nullableDictionary: JSON?
    var nullableArray: JSON?
    var arrayOfBools: JSON
    var dictionary: JSON
    var arrayOfDifferentTypes: JSON
}

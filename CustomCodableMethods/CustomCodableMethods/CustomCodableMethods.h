//
//  CustomCodableMethods.h
//  CustomCodableMethods
//
//  Created by Pavel Smelovsky on 11/06/2019.
//  Copyright © 2019 Pavel Smelovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CustomCodableMethods.
FOUNDATION_EXPORT double CustomCodableMethodsVersionNumber;

//! Project version string for CustomCodableMethods.
FOUNDATION_EXPORT const unsigned char CustomCodableMethodsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CustomCodableMethods/PublicHeader.h>



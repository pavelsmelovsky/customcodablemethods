//
//  JSON+Comparable.swift
//  CustomCodableMethods
//
//  Created by Pavel Smelovsky on 12/06/2019.
//  Copyright © 2019 Pavel Smelovsky. All rights reserved.
//

import Foundation

extension JSON: Equatable {
    public static func == (lhs: JSON, rhs: JSON) -> Bool {
        switch (lhs, rhs) {
        case (.null, .null): return true
        case let (.int(lvalue), .int(rvalue)):
            return lvalue == rvalue
        case let (.uint(lvalue), .uint(rvalue)):
            return lvalue == rvalue
        case let (.double(lvalue), .double(rvalue)):
            return lvalue == rvalue
        case let (.bool(lvalue), .bool(rvalue)):
            return lvalue == rvalue
        case let (.string(lvalue), .string(rvalue)):
            return lvalue == rvalue
        case let (.array(lvalue), .array(rvalue)):
            return self.array(lvalue, isEqualTo: rvalue)
        case let (.dictionary(lvalue), .dictionary(rvalue)):
            return self.dictionary(lvalue, isEqualTo: rvalue)
        default:
            return false
        }
    }

    private static func array(_ array1: [JSON], isEqualTo array2: [JSON]) -> Bool {
        guard array1.count == array2.count else { return false }

        return array1.elementsEqual(array2)
    }

    private static func dictionary(_ dict1: [String: JSON], isEqualTo dict2: [String: JSON]) -> Bool {
        guard dict1.count == dict2.count else { return false }

        return dict1.isEqual(to: dict2, comparator: { (item1, item2) -> Bool in
            return item1.value == item2.value
        })
    }
}

extension Dictionary {
    func isEqual(to other: Dictionary, comparator: (Element, Element) -> Bool) -> Bool {
        guard self.count == other.count else { return false }

        for item in self {
            guard let index = other.index(forKey: item.key) else { return false }
            guard comparator(item, other[index]) else { return false }
        }
        return true
    }
}

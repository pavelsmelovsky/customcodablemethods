//
//  JSON+Encodable.swift
//  CustomCodableMethods
//
//  Created by Pavel Smelovsky on 11/06/2019.
//  Copyright © 2019 Pavel Smelovsky. All rights reserved.
//

import Foundation

extension JSON: Encodable {
    public func encode(to encoder: Encoder) throws {
        switch self {
        case .null: try self.encodeNil(to: encoder)
        case let .int(value): try self.encode(value, to: encoder)
        case let .uint(value): try self.encode(value, to: encoder)
        case let .double(value): try self.encode(value, to: encoder)
        case let .bool(value): try self.encode(value, to: encoder)
        case let .string(value): try self.encode(value, to: encoder)

        case let .array(array): try self.encode(array, to: encoder)
        case let .dictionary(dict): try self.encode(dict, to: encoder)
        }
    }

    private func encodeNil(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }

    private func encode<T: Encodable>(_ value: T, to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(value)
    }

    private func encode<T: Encodable>(_ array: [T], to encoder: Encoder) throws {
        var container = encoder.unkeyedContainer()
        for item in array {
            try container.encode(item)
        }
    }

    private func encode<T: Encodable>(_ dictionary: [String: T], to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: JSONCodingKeys.self)
        try dictionary.forEach { (item) in
            if let key = JSONCodingKeys(stringValue: item.key) {
                try container.encode(item.value, forKey: key)
            }
        }
    }
}

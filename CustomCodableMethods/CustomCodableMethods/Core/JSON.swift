//
//  JSON.swift
//  CustomCodableMethods
//
//  Created by Pavel Smelovsky on 11/06/2019.
//  Copyright © 2019 Pavel Smelovsky. All rights reserved.
//

import Foundation

public enum JSON: Decodable {
    case null
    case bool(Bool)
    case int(Int)
    case uint(UInt)
    case double(Double)
    case string(String)
    indirect case array([JSON])
    indirect case dictionary([String: JSON])

    public init(from decoder: Decoder) throws {
        if var container = try? decoder.container(keyedBy: JSONCodingKeys.self) {
            self = JSON(container: &container)
        } else if var container = try? decoder.unkeyedContainer() {
            self = JSON(container: &container)
        } else {
            throw DecodingError.dataCorrupted(DecodingError.Context.init(codingPath: decoder.codingPath, debugDescription: "Data corrupted"))
        }
    }

    private init(container: inout KeyedDecodingContainer<JSONCodingKeys>) {
        var dict = [String: JSON]()

        for key in container.allKeys {
            if let isNil = try? container.decodeNil(forKey: key), isNil {
                dict[key.stringValue] = .null
            } else if let value = try? container.decode(Int.self, forKey: key) {
                dict[key.stringValue] = .int(value)
            } else if let value = try? container.decode(UInt.self, forKey: key) {
                dict[key.stringValue] = .uint(value)
            } else if let value = try? container.decode(Double.self, forKey: key) {
                dict[key.stringValue] = .double(value)
            } else if let value = try? container.decode(Bool.self, forKey: key) {
                dict[key.stringValue] = .bool(value)
            } else if let value = try? container.decode(String.self, forKey: key) {
                dict[key.stringValue] = .string(value)
            } else if var value = try? container.nestedContainer(keyedBy: JSONCodingKeys.self, forKey: key) {
                dict[key.stringValue] = JSON(container: &value)
            } else if var value = try? container.nestedUnkeyedContainer(forKey: key) {
                dict[key.stringValue] = JSON(container: &value)
            }
        }

        self = .dictionary(dict)
    }

    private init(container: inout UnkeyedDecodingContainer) {
        var array = [JSON]()
        while !container.isAtEnd {
            if let isNil = try? container.decodeNil(), isNil {
                array.append(.null)
            } else if let value = try? container.decode(Int.self) {
                array.append(.int(value))
            } else if let value = try? container.decode(UInt.self) {
                array.append(.uint(value))
            } else if let value = try? container.decode(Double.self) {
                array.append(.double(value))
            } else if let value = try? container.decode(Bool.self) {
                array.append(.bool(value))
            } else if let value = try? container.decode(String.self) {
                array.append(.string(value))
            } else if var value = try? container.nestedContainer(keyedBy: JSONCodingKeys.self) {
                array.append(JSON(container: &value))
            } else if var value = try? container.nestedUnkeyedContainer() {
                array.append(JSON(container: &value))
            }
        }

        self = .array(array)
    }
}

//
//  JSONCodingKey.swift
//  CustomCodableMethods
//
//  Created by Pavel Smelovsky on 11/06/2019.
//  Copyright © 2019 Pavel Smelovsky. All rights reserved.
//

import Foundation

// Inspired by https://gist.github.com/mbuchetics/c9bc6c22033014aa0c550d3b4324411a

internal struct JSONCodingKeys: CodingKey {
    var stringValue: String

    init?(stringValue: String) {
        self.stringValue = stringValue
    }

    var intValue: Int?

    init?(intValue: Int) {
        self.init(stringValue: "\(intValue)")
        self.intValue = intValue
    }
}

//
//  Encodable+Any.swift
//  CustomCodableMethods
//
//  Created by Pavel Smelovsky on 11/06/2019.
//  Copyright © 2019 Pavel Smelovsky. All rights reserved.
//

import Foundation

public extension KeyedEncodingContainer {
    mutating func encodeIfPresent(_ value: [Any]?, forKey key: K) throws {
        if let value = value {
            try self.encode(value, forKey: key)
        } else {
            try self.encodeNil(forKey: key)
        }
    }

    mutating func encode(_ value: [Any], forKey key: K) throws {
        var container = self.nestedUnkeyedContainer(forKey: key)
        for item in value {
            if let obj = item as? String {
                try container.encode(obj)
            } else if let obj = item as? Bool {
                try container.encode(obj)
            } else if let obj = item as? Int {
                try container.encode(obj)
            } else if let obj = item as? UInt {
                try container.encode(obj)
            } else if let obj = item as? Float {
                try container.encode(obj)
            } else if let obj = item as? Double {
                try container.encode(obj)
            } else if let obj = item as? [String: Any] {
                try container.encode(obj)
            } else if let obj = item as? [Any] {
                try container.encode(obj)
            }
        }
    }

    mutating func encodeIfPresent(_ value: [String: Any]?, forKey key: K) throws {
        if let value = value {
            try self.encode(value, forKey: key)
        } else {
            try self.encodeNil(forKey: key)
        }
    }

    mutating func encode(_ value: [String: Any], forKey key: K) throws {
        var container = self.nestedContainer(keyedBy: JSONCodingKeys.self, forKey: key)
        for item in value {
            guard let itemKey = JSONCodingKeys(stringValue: item.key) else { continue }

            if let obj = item.value as? String {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? Bool {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? Int {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? UInt {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? Float {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? Double {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? [String: Any] {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? [Any] {
                try container.encode(obj, forKey: itemKey)
            }
        }
    }
}

public extension UnkeyedEncodingContainer {
    mutating func encode(_ value: [Any]) throws {
        var container = self.nestedUnkeyedContainer()
        for item in value {
            if let obj = item as? String {
                try container.encode(obj)
            } else if let obj = item as? Bool {
                try container.encode(obj)
            } else if let obj = item as? Int {
                try container.encode(obj)
            } else if let obj = item as? Float {
                try container.encode(obj)
            } else if let obj = item as? Double {
                try container.encode(obj)
            } else if let obj = item as? UInt {
                try container.encode(obj)
            } else if let obj = item as? [String: Any] {
                try container.encode(obj)
            } else if let obj = item as? [Any] {
                try container.encode(obj)
            }
        }
    }

    mutating func encode(_ value: [String: Any]) throws {
        var container = self.nestedContainer(keyedBy: JSONCodingKeys.self)
        for item in value {
            guard let itemKey = JSONCodingKeys(stringValue: item.key) else { continue }

            if let obj = item.value as? String {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? Bool {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? Int {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? Float {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? Double {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? UInt {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? [String: Any] {
                try container.encode(obj, forKey: itemKey)
            } else if let obj = item.value as? [Any] {
                try container.encode(obj, forKey: itemKey)
            }
        }
    }
}

//
//  JSON+Descriptions.swift
//  CustomCodableMethods
//
//  Created by Pavel Smelovsky on 12/06/2019.
//  Copyright © 2019 Pavel Smelovsky. All rights reserved.
//

import Foundation

extension JSON: CustomStringConvertible {
    public var description: String {
        switch self {
        case .null:
            return ".null"
        case let .int(value):
            return ".int(\(value))"
        case let .uint(value):
            return ".uint(\(value))"
        case let .double(value):
            return ".double(\(value))"
        case let .bool(value):
            return ".bool(\(value))"
        case let .string(value):
            return ".string(\(value))"
        case let .array(value):
            return ".array(\(value.count))"
        case let .dictionary(value):
            return ".dictionary(\(value.count))"
        }
    }
}
